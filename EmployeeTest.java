
public class EmployeeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp1 = new Employee("Hieu", "Nguyen", 100);
		Employee emp2 = new Employee("Hoan", "Nguyen", 101);
		
		System.out.println("emp1's yearly salary is: " + emp1.getSalary() * 12);
		System.out.println("emp2's yearly salary is: " + emp2.getSalary() * 12);
		
		emp1.setSalary(emp1.getSalary() + emp1.getSalary()* 0.1);
		emp2.setSalary(emp2.getSalary() + emp2.getSalary()* 0.1);
		
		System.out.println("emp1's yearly salary ,which raise 10%, is: " + emp1.getSalary() * 12);
		System.out.println("emp2's yearly salary ,which raise 10%, is: " + emp2.getSalary() * 12);
		
	}

}
